﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

using SQLite;
using UIKit;
using Xamarin.Forms;
using Semana12.Datos;
using Semana12.iOS;
using System.IO;

[assembly: Dependency(typeof(SQLiteDB))]
namespace Semana12.iOS
{
    class SQLiteDB : IConexion
    {
        public SQLiteAsyncConnection GetConnection()
        {
            var ruta = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            //Se crea la Base de datos
            var path = Path.Combine(ruta, "AgenciaViajes.db3");
            return new SQLiteAsyncConnection(path);
        }
    }
}