﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Semana12.Tablas
{
    class Direccion
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(255)]
        public string Calle { get; set; }
        [MaxLength(10)]
        public string NumInt { get; set; }
        [MaxLength(10)]
        public string NumExt { get; set; }
        [MaxLength(255)]
        public string Colonia { get; set; }
        [MaxLength(255)]
        public string Ciudad { get; set; }
        [MaxLength(255)]
        public string Estado { get; set; }
        [MaxLength(255)]
        public string Pais { get; set; }
        public int Postal { get; set; }
    }
}
