﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Semana12.Tablas
{
    class Hotel
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Direccion { get; set; }
        [MaxLength(255)]
        public string Llegada { get; set; }
        public int Noches { get; set; }
        [MaxLength(255)]
        public string Habitacion { get; set; }
        [MaxLength(255)]
        public string Detalles { get; set; }
    }
}
