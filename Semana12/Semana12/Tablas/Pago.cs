﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Semana12.Tablas
{
    class Pago
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Reservacion { get; set; }
        public double Cantidad { get; set; }
        [MaxLength(10)]
        public string Fecha { get; set; }
        [MaxLength(50)]
        public string TipoPago { get; set; }
        [MaxLength(255)]
        public string Detalles { get; set; }
    }
}
