﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Semana12.Tablas
{
    class Cliente
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(255)]
        public string Nombre { get; set; }
        [MaxLength(255)]
        public string Apellidos { get; set; }
        [MaxLength(255)]
        public string Correo { get; set; }
        public string Pass { get; set; }
    }
}
