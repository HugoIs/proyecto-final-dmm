﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Semana12.Tablas
{
    class Vuelo
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        [MaxLength(255)]
        public DateTime Partida { get; set; }
        [MaxLength(255)]
        public DateTime Llegada { get; set; }
        [MaxLength(255)]
        public string Aerolinea { get; set; }
        public int Asiento { get; set; }
    }
}
