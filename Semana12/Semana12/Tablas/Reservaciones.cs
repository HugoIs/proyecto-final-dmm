﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Semana12.Tablas
{
    class Reservaciones
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Hotel { get; set; }
        public int Vuelo { get; set; }
        [MaxLength(255)]
        public string Detalles { get; set; }
        [MaxLength(10)]
        public string Fecha { get; set; }
      

    }
}
