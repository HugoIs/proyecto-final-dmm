﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Semana12.Tablas
{
    class ClienteReserva
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int Cliente { get; set; }
        public int Reservacion { get; set; }
    }
}
