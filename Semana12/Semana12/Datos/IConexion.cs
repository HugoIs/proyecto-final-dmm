﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace Semana12.Datos
{
    public interface IConexion
    {
        SQLiteAsyncConnection GetConnection();
    }
}
