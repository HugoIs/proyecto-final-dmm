﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using System.Collections.ObjectModel;
using System.IO;
using Semana12.Datos;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalleVuelos : ContentPage
    {
        public int IdSeleccionado, AsiSeleccionado;
        public string AerSeleccionado;
        public DateTime ParSeleccionado, LegSeleccionado;
        private SQLiteAsyncConnection conexion;
        IEnumerable<Vuelo> ResultadoDelete;
        IEnumerable<Vuelo> ResultadoUpdate;
        public DetalleVuelos(int ID, DateTime par, DateTime leg, string aer, int asi)
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            IdSeleccionado = ID;
            ParSeleccionado = par;
            LegSeleccionado = leg;
            AerSeleccionado = aer;
            AsiSeleccionado = asi;
            actualizar.Clicked += Actualizar_Clicked;
            eliminar.Clicked += Eliminar_Clicked;
        }
        private static IEnumerable<Vuelo> Delete(SQLiteConnection db, int id)
        {
            return db.Query<Vuelo>("DELETE FROM Vuelo where Id = ?", id);
        }
        private static IEnumerable<Vuelo> Update(SQLiteConnection db, int id, DateTime par, DateTime leg, string aer, int asi)
        {
            return db.Query<Vuelo>("UPDATE Vuelo SET Aerolinea = ?, Partida = ?," +
                "Llegada = ?, Asiento = ? where Id = ? ", aer, par, leg, asi, id);
        }
        private void Eliminar_Clicked(object sender, EventArgs e)
        {
            var rutaBD = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            "AgenciaViajes.db3");
            var db = new SQLiteConnection(rutaBD);
            ResultadoDelete = Delete(db, IdSeleccionado);
            DisplayAlert("Confirmación", "El contacto se eliminó correctamente", "OK");
            Limpiar();
        }
        private void Actualizar_Clicked(object sender, EventArgs e)
        {
            var rutaBD = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            "AgenciaViajes.db3");
            var db = new SQLiteConnection(rutaBD);
            ResultadoUpdate = Update(db, IdSeleccionado, txtPartida.Date, txtLlegada.Date, txtAerolinea.Text, int.Parse(txtAsiento.Text));
            DisplayAlert("Confirmación", "El contacto se actualizó correctamente", "OK");
        }
        private void Limpiar()
        {
            lblMensaje.Text = "";
            txtPartida.Date = DateTime.Today;
            txtLlegada.Date = DateTime.Today;
            txtAerolinea.Text = "";
            txtAsiento.Text = "";
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            lblMensaje.Text = " Vuelo número " + IdSeleccionado;
            txtPartida.Date = ParSeleccionado;
            txtLlegada.Date = LegSeleccionado;
            txtAerolinea.Text = AerSeleccionado;
            txtAsiento.Text = AsiSeleccionado.ToString();
        }
    }
}