﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using System.Collections.ObjectModel;
using System.IO;
using Semana12.Datos;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetallesDirecciones : ContentPage
    {
        public int IdSeleccionado, PosSeleccionado;
        public string CalSeleccionado, IntSeleccionado, ExtSeleccionado, ColSeleccionado, CiuSeleccionado, EstSeleccionado, PaiSeleccionado;
        private SQLiteAsyncConnection conexion;
        IEnumerable<Direccion> ResultadoDelete;
        IEnumerable<Direccion> ResultadoUpdate;
        public DetallesDirecciones(int ID, string cal, string numInt, string numExt, string Colonia, string Ciudad, string Estado, string Pais, int Postal)
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            IdSeleccionado = ID;
            CalSeleccionado = cal;
            IntSeleccionado = numInt;
            ExtSeleccionado = numExt;
            ColSeleccionado = Colonia;
            CiuSeleccionado = Ciudad;
            EstSeleccionado = Estado;
            PaiSeleccionado = Pais;
            PosSeleccionado = Postal;
            actualizar.Clicked += Actualizar_Clicked;
            eliminar.Clicked += Eliminar_Clicked;
        }
        private static IEnumerable<Direccion> Delete(SQLiteConnection db, int id)
        {
            return db.Query<Direccion>("DELETE FROM Direccion where Id = ?", id);
        }
        private static IEnumerable<Direccion> Update(SQLiteConnection db, string cal, string numInt,
            string numExt, string Colonia, string Ciudad, string Estado, string Pais, int Postal, int id)
        {
            return db.Query<Direccion>("UPDATE Direccion SET Calle = ?, NumInt = ?, NumExt = ?," +
                " Colonia = ?, Ciudad = ?, Estado = ?, Pais = ?," +
                "Postal = ? where Id = ? ", cal, numInt, numExt, Colonia, Ciudad, Estado, Pais, Postal, id);
        }
        private void Eliminar_Clicked(object sender, EventArgs e)
        {
            var rutaBD = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            "AgenciaViajes.db3");
            var db = new SQLiteConnection(rutaBD);
            ResultadoDelete = Delete(db, IdSeleccionado);
            DisplayAlert("Confirmación", "El contacto se eliminó correctamente", "OK");
            Limpiar();
        }
        private void Actualizar_Clicked(object sender, EventArgs e)
        {
            var rutaBD = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            "AgenciaViajes.db3");
            var db = new SQLiteConnection(rutaBD);
            ResultadoUpdate = Update(db, txtCalle.Text, txtNumInt.Text, txtNumExt.Text,
                txtColonia.Text, txtCiudad.Text, txtEstado.Text, txtPais.Text, int.Parse(txtPostal.Text), IdSeleccionado);
            DisplayAlert("Confirmación", "El contacto se actualizó correctamente", "OK");
        }
        private void Limpiar()
        {
            lblMensaje.Text = "";
            txtCalle.Text = "";
            txtNumInt.Text = "";
            txtNumExt.Text = "";
            txtColonia.Text = "";
            txtCiudad.Text = "";
            txtEstado.Text = "";
            txtPais.Text = "";
            txtPostal.Text = "";
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            lblMensaje.Text = "Estas viendo el documento " + IdSeleccionado;
            txtCalle.Text = CalSeleccionado;
            txtNumInt.Text = IntSeleccionado;
            txtNumExt.Text = ExtSeleccionado;
            txtColonia.Text = ColSeleccionado;
            txtCiudad.Text = CiuSeleccionado;
            txtEstado.Text = EstSeleccionado;
            txtPais.Text = PaiSeleccionado;
            txtPostal.Text = PosSeleccionado.ToString();
        }
    }
}