﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using System.IO;
using Semana12.Datos;
using Semana12.Vistas;

namespace Semana12.Tablas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Hoteles : ContentPage
    {
        private SQLiteAsyncConnection conexion;
        public Hoteles()
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            busca.Clicked += Busca_Clicked;
            registro.Clicked += Registro_Clicked;
        }
        private void Registro_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegistroHoteles());
        }
        private void Busca_Clicked(object sender, EventArgs e)
        {
            try
            {
                var rutaBD = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "AgenciaViajes.db3");
                var db = new SQLiteConnection(rutaBD);
                db.CreateTable<Hotel>();
                IEnumerable<Hotel> resultado = SELECT_WHERE(db, txtNombre.Text);
                if (resultado.Count() > 0)
                {
                    Navigation.PushAsync(new ConsultaHoteles());
                    DisplayAlert("Aviso", "Se encontraron hoteles", "oki");
                }
                else
                {
                    DisplayAlert("Aviso", "No se hayaron hoteles", "ok :(");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static IEnumerable<Hotel> SELECT_WHERE(SQLiteConnection db, string detalle)
        {
            return db.Query<Hotel>("SELECT * FROM Hotel WHERE Detalles=?", detalle);
        }
    }
}