﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Inicio : ContentPage
    {
        public Inicio()
        {
            InitializeComponent();
            boton.Clicked += Boton_Clicked;
        }

        private void Boton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Bienvenido());
        }
    }
}