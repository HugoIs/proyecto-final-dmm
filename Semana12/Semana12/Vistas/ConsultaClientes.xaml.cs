﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using System.Collections.ObjectModel;
using System.IO;
using Semana12.Datos;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsultaClientes : ContentPage
    {
        private SQLiteAsyncConnection conexion;
        private ObservableCollection<Cliente> TablaContacto;
        public ConsultaClientes()
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            ListaClientes.ItemSelected += ListaContactos_ItemSelected;
        }
        private void ListaContactos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var Obj = (Cliente)e.SelectedItem;
            var item = Obj.Id.ToString();
            var nom = Obj.Nombre;
            var ap = Obj.Apellidos;
            var pass = Obj.Pass;
            var cor = Obj.Correo;
            int ID = Convert.ToInt32(item);
            try
            {
                Navigation.PushAsync(new DetalleClientes(ID, nom, ap, cor, pass));
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected async override void OnAppearing()
        {
            var ResulRegistros = await conexion.Table<Cliente>().ToListAsync();
            TablaContacto = new ObservableCollection<Cliente>(ResulRegistros);
            ListaClientes.ItemsSource = TablaContacto;
            base.OnAppearing();
        }
    }
}