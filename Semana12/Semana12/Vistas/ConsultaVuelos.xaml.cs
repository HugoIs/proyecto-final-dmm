﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using System.Collections.ObjectModel;
using System.IO;
using Semana12.Datos;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsultaVuelos : ContentPage
    {
        private SQLiteAsyncConnection conexion;
        private ObservableCollection<Vuelo> TablaContacto;
        public ConsultaVuelos()
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            ListaVuelos.ItemSelected += ListaContactos_ItemSelected;
        }
        private void ListaContactos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var Obj = (Vuelo)e.SelectedItem;
            var item = Obj.Id.ToString();
            var par = Obj.Partida;
            var leg = Obj.Llegada;
            var aer = Obj.Aerolinea;
            var asi = Obj.Asiento;
            int ID = Convert.ToInt32(item);
            try
            {
                Navigation.PushAsync(new DetalleVuelos(ID, par, leg, aer, asi));
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected async override void OnAppearing()
        {
            var ResulRegistros = await conexion.Table<Vuelo>().ToListAsync();
            TablaContacto = new ObservableCollection<Vuelo>(ResulRegistros);
            ListaVuelos.ItemsSource = TablaContacto;
            base.OnAppearing();
        }
    }
}