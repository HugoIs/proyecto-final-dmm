﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using Semana12.Datos;
using Semana12.Vistas;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistroClientes : ContentPage
    {
        private SQLiteAsyncConnection conexion;
        public RegistroClientes()
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            Guardar.Clicked += Guardar_Clicked;
        }

        private void Guardar_Clicked(object sender, EventArgs e)
        {
            var DatosCliente = new Cliente
            {
                Nombre = txtNombre.Text,
                Apellidos = txtApellidos.Text,
                Correo = txtCorreo.Text,
                Pass = txtPass.Text
            };

            conexion.InsertAsync(DatosCliente);
            limpiarFormulario();
            DisplayAlert("Confirmación", "El cliente se registró correctamente", "OK");
        }
        void limpiarFormulario()
        {
            txtNombre.Text = "";
            txtApellidos.Text = "";
            txtCorreo.Text = "";
            txtPass.Text = "";
        }
    }
}