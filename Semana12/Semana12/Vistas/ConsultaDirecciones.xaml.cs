﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using System.Collections.ObjectModel;
using System.IO;
using Semana12.Datos;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConsultaDirecciones : ContentPage
    {
        private SQLiteAsyncConnection conexion;
        private ObservableCollection<Direccion> TablaDireccion;
        public ConsultaDirecciones()
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            ListaDirecciones.ItemSelected += ListaDirecciones_ItemSelected;
        }
        private void ListaDirecciones_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var Obj = (Direccion)e.SelectedItem;
            var item = Obj.Id.ToString();
            var Calle = Obj.Calle;
            var NumInt = Obj.NumInt;
            var NumExt = Obj.NumExt;
            var Colonia = Obj.Colonia;
            var Ciudad = Obj.Ciudad;
            var Estado = Obj.Estado;
            var Pais = Obj.Pais;
            var Postal = Obj.Postal;
            int ID = Convert.ToInt32(item);
            try
            {
                Navigation.PushAsync(new DetallesDirecciones(ID, Calle, NumInt, NumExt, Colonia, Ciudad, Estado, Pais, Postal));
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected async override void OnAppearing()
        {
            var ResulRegistros = await conexion.Table<Direccion>().ToListAsync();
            TablaDireccion = new ObservableCollection<Direccion>(ResulRegistros);
            ListaDirecciones.ItemsSource = TablaDireccion;
            base.OnAppearing();
        }
    }
}