﻿using Semana12.Tablas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Bienvenido : ContentPage
    {
        public Bienvenido()
        {
            InitializeComponent();
            cliente.Clicked += Cliente_Clicked;
            direccion.Clicked += Direccion_Clicked;
            vuelo.Clicked += Vuelo_Clicked;
        }

        private void Cliente_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Clientes());
        }

        private void Direccion_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Direcciones());
        }
        private void Vuelo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Vuelos());
        }
    }
}