﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using System.Collections.ObjectModel;
using System.IO;
using Semana12.Datos;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetalleClientes : ContentPage
    {
        public int IdSeleccionado;
        public string NomSeleccionado, ApSeleccionado, CorSeleccionado, PasSeleccionado;
        private SQLiteAsyncConnection conexion;
        IEnumerable<Cliente> ResultadoDelete;
        IEnumerable<Cliente> ResultadoUpdate;
        public DetalleClientes(int id, string nom, string ap, string cor, string pass)
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            IdSeleccionado = id;
            NomSeleccionado = nom;
            ApSeleccionado = ap;
            CorSeleccionado = cor;
            PasSeleccionado = pass;
            actualizar.Clicked += Actualizar_Clicked;
            eliminar.Clicked += Eliminar_Clicked;
        }
        private static IEnumerable<Cliente> Delete(SQLiteConnection db, int id)
        {
            return db.Query<Cliente>("DELETE FROM Cliente where Id = ?", id);
        }
        private static IEnumerable<Cliente> Update(SQLiteConnection db, string nombre,
            string apellidos, string correo, string pass, int id)
        {
            return db.Query<Cliente>("UPDATE Cliente SET Nombre = ?, Apellidos = ?," +
                "Correo = ?, Pass = ? where Id = ? ", nombre, apellidos, correo, pass, id);
        }
        private void Eliminar_Clicked(object sender, EventArgs e)
        {
            var rutaBD = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            "AgenciaViajes.db3");
            var db = new SQLiteConnection(rutaBD);
            ResultadoDelete = Delete(db, IdSeleccionado);
            DisplayAlert("Confirmación", "El contacto se eliminó correctamente", "OK");
            Limpiar();
        }
        private void Actualizar_Clicked(object sender, EventArgs e)
        {
            var rutaBD = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            "AgenciaViajes.db3");
            var db = new SQLiteConnection(rutaBD);
            ResultadoUpdate = Update(db, txtNombre.Text, txtApellidos.Text, txtCorreo.Text, txtPass.Text,
            IdSeleccionado);
            DisplayAlert("Confirmación", "El contacto se actualizó correctamente", "OK");
        }
        private void Limpiar()
        {
            lblMensaje.Text = "";
            txtNombre.Text = "";
            txtApellidos.Text = "";
            txtCorreo.Text = "";
            txtPass.Text = "";
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            lblMensaje.Text = " Hola " + NomSeleccionado;
            txtNombre.Text = NomSeleccionado;
            txtApellidos.Text = ApSeleccionado;
            txtCorreo.Text = CorSeleccionado;
            txtPass.Text = PasSeleccionado;
        }
    }
}