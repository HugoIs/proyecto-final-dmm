﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using Semana12.Datos;
using Semana12.Vistas;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistroDirecciones : ContentPage
    {
        private SQLiteAsyncConnection conexion;
        public RegistroDirecciones()
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            Guardar.Clicked += Guardar_Clicked;
        }

        private void Guardar_Clicked(object sender, EventArgs e)
        {
            var DatosDireccion = new Direccion
            {
                Calle = txtCalle.Text,
                NumInt = txtNumInt.Text,
                NumExt = txtNumExt.Text,
                Colonia = txtColonia.Text,
                Ciudad = txtCiudad.Text,
                Estado = txtEstado.Text,
                Pais = txtPais.Text,
                Postal = int.Parse(txtPostal.Text)
            };

            conexion.InsertAsync(DatosDireccion);
            limpiarFormulario();
            DisplayAlert("Confirmación", "Se añadio la dirección", "OK");
        }
        void limpiarFormulario()
        {
            txtCalle.Text = "";
            txtNumInt.Text = "";
            txtNumExt.Text = "";
            txtColonia.Text = "";
            txtCiudad.Text = "";
            txtEstado.Text = "";
            txtPais.Text = "";
            txtPostal.Text = "";
        }
    }
}