﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using System.IO;
using Semana12.Datos;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Clientes : ContentPage
    {
        private SQLiteAsyncConnection conexion;
        public Clientes()
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            busca.Clicked += Busca_Clicked;
            registro.Clicked += Registro_Clicked;
        }
        private void Registro_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new RegistroClientes());
        }
        private void Busca_Clicked(object sender, EventArgs e)
        {
            try
            {
                var rutaBD = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "AgenciaViajes.db3");
                var db = new SQLiteConnection(rutaBD);
                db.CreateTable<Cliente>();
                IEnumerable<Cliente> resultado = SELECT_WHERE(db, txtNombre.Text);
                if (resultado.Count() > 0)
                {
                    Navigation.PushAsync(new ConsultaClientes());
                    DisplayAlert("Aviso", "Si hay clientes que coincidan", "ok");
                }
                else
                {
                    DisplayAlert("Aviso", "No hay clientes que coincidan", "ok :(");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static IEnumerable<Cliente> SELECT_WHERE(SQLiteConnection db, string nombre)
        {
            return db.Query<Cliente>("SELECT * FROM Cliente WHERE Nombre=?", nombre);
        }
    }
}