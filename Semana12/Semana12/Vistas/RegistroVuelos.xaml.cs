﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SQLite;
using Semana12.Tablas;
using Semana12.Datos;

namespace Semana12.Vistas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistroVuelos : ContentPage
    {
        private SQLiteAsyncConnection conexion;
        public RegistroVuelos()
        {
            InitializeComponent();
            conexion = DependencyService.Get<IConexion>().GetConnection();
            Guardar.Clicked += Guardar_Clicked;
        }

        private void Guardar_Clicked(object sender, EventArgs e)
        {
            var DatosVuelo = new Vuelo
            {
                Partida = txtPartida.Date,
                Llegada = txtLlegada.Date,
                Aerolinea = txtAerolinea.Text,
                Asiento = int.Parse(txtAsiento.Text)
            };

            conexion.InsertAsync(DatosVuelo);
            limpiarFormulario();
            DisplayAlert("Confirmación", "El Vuelo se registró correctamente", "OK");
        }
        void limpiarFormulario()
        {
            txtPartida.Date = DateTime.Today;
            txtLlegada.Date = DateTime.Today;
            txtAerolinea.Text = "";
            txtAsiento.Text = "";
        }
    }
}